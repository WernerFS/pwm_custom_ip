set_property PACKAGE_PIN T22 [get_ports pwm_0]
set_property PACKAGE_PIN Y11 [get_ports pwm_1]

set_property IOSTANDARD LVCMOS33 [get_ports pwm_0]
set_property IOSTANDARD LVCMOS33 [get_ports pwm_1]
